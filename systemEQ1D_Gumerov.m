function dy = systemEQ1D_Gumerov( t, y )

global R01 R02 ap rho_l rho_p P0 gamma sigma Pg0 Ap omega;

ab = y(1);
ap = y(2);
wb = y(3);
wp = y(4);
wp = 0;
Ub = y(5);
Up = y(6);
rb = y(7);
rp = y(8);

U = Up - Ub;
d = rp - rb;
% U = Ub - Up;
% d = rb - rp;
if d < 0
    disp('d < 0 became');
end;

eps_b = ab/d;
eps_p = ap/d;
rho = rho_p/rho_l;
rho = 0;


% p_inf = P0 - Ap*sin(omega*t);%p_inf = -Ap*sin(omega*t);
p_inf = P0;
p_lb = Pg0*(R01/ab)^(3*gamma)-2*sigma/ab;
p_lp = Pg0*(R02/ap)^(3*gamma)-2*sigma/ap;

% Alb = -3/2*eps_b^2*(wb-eps_b*(-U)*(-1))*Ub + eps_b^2*(wb+3/2*eps_b*Ub*(-1))*(-U) + ...
%     eps_b*(2*wb^2-3*eps_b*wb*(-U)*(-1)+3/2*eps_b^2*Ub*(-U)+9/2*eps_b*wb*Ub*(-1)-15/2*eps_b^2*Ub*(-1)*(-U)*(-1))*(-1);
% Alp = -3/2*eps_b^2*(wb-eps_b*U)*Ub + eps_b^2*(wb+3/2*eps_b*Ub)*U + ...
%     eps_b*(2*wb^2-3*eps_b*wb*U+3/2*eps_b^2*Ub*U+9/2*eps_b*wb*Ub-15/2*eps_b^2*Ub*U);
% Alb = -3/2*eps_p^2*(wp - eps_p*dot(-U,-1))*Up + eps_p^2*(wp+3/2*eps_p*dot(Up,-1))*-U ...
%    + eps_p*(2*wp^2-3*eps_p*wp*dot(-U,-1) + 3/2*eps_p^2*dot(Up,-U)+9/2*eps_p*wp*dot(Up,-1)-15/2*eps_p^2*dot(Up,-1)*dot(-U,-1))*-1;
% Alb = -3/2*eps_p^2*(wp - eps_p*U)*Up - eps_p^2*(wp-3/2*eps_p*Up)*U ...
%     - eps_p*(2*wp^2-3*eps_p*wp*U  ...
%     - 3/2*eps_p^2*Up*U-9/2*eps_p*wp*Up+15/2*eps_p^2*Up*U);
Alb = -2*eps_p*wp^2 + eps_p^2*wp*(3*Up+2*U) - 3*eps_p^3*Up*U;
Alp =  2*eps_b*wb^2 + eps_b^2*wb*(3*Ub-2*U) - 3*eps_b^3*U*Ub;
% Alp = -3/2*eps_b^2*(wb - eps_b*dot(U,1))*Ub + eps_b^2*(wb+3/2*eps_b*dot(Ub,1))*U ...
%    + eps_b*(2*wb^2-3*eps_b*wb*dot(U,1) + 3/2*eps_b^2*dot(Ub,U)+9/2*eps_b*wb*dot(Ub,1)-15/2*eps_b^2*dot(Ub,1)*dot(U,1))*1;


Ulb = -eps_p^2*wp + eps_p^3*Up;
Ulp =  eps_b^2*wb + eps_b^3*Ub;
Qb = -3*wb*(Ub-Ulb);
Qp = -3*wp*(Up-Ulp);
Qbv = -3/2*wb^2 + 1/4*abs(Ub-Ulb)^2 + (p_lb - p_inf)/rho_l;
Qpv = -3/2*wp^2 + 1/4*abs(Up-Ulp)^2 + (p_lp - p_inf)/rho_l;

Rb = 3*eps_b*Alb + Qb;
Rp = 3*eps_p*Alp + Qp;

M = [ab               -3*ab*eps_p^3;
     -3*ap*eps_b^3    (1+2*rho)*ap
    ];
F  = [ Rb;
      3*ap*eps_b^2*Qbv/ab + Rp];
dU = M\F;

dy = zeros(6,1);
dy(1) = wb;
dy(2) = wp;
dy(3) = Qbv/ab;
dy(4) = 0;%Qpv/ap;
dy(5) = dU(1);
dy(6) = dU(2);
dy(7) = Ub;
dy(8) = Up;


end

