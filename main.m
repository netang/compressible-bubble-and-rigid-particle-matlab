global k R01 R02 ap d rho_l rho_p V02 P0 gamma sigma Pg0 Ap omega;
c = 1;
k = 10; %��� ������ k, ��� ������ ��������� ����� ��������� � ��������
R01 = 1e-5; % ������ ���������
R02 = c*(1e-5); % ������ �������
ap = R02;
d = R01+R02+k*R01; %��������� �� ������ �������� �� ������ �������
rho_l = 1000; % ��������� ��������
rho_p = 0;%2000; % ��������� �������, rho1 �� ����������, �.�. ������������ ��.
V02 = (4/3)*pi*R02^3; %�����  ������� (� ������ ������ �� �� ��������)
P0 = 1e+5; %����������� �������� (�������� � ��������)
gamma = 1.4; %����������� ���������
sigma = 0.073;
Pg0 = P0+2*sigma/R01;
Ap = P0/2;
nu = 200e+3;
omega = 2*pi*nu;
NT = 5;
tmax = NT/nu;
Nt=5001;
Toutput=linspace(0, tmax, Nt);
time = Toutput*nu;


options = odeset('RelTol',1e-9);
 [EQ_1D_Gumerov_T,EQ_1D_Gumerov_Y] = ode45(@systemEQ1D_Gumerov, Toutput, [R01 R02 0 0 1 -1 0 d], options); 
%[EQ_3D_Gumerov_T, EQ_3D_Gumerov_Y] = ode45(@systemEQ3D_Gumerov, Toutput, [R01 R02 0 0 [0 0 0] [0 0 0] [0 0 0] [d 0 0]], options); 

 [T,Y1] = ode45(@SimpleSystem,Toutput,[R01 0], options);
 [T,Y2] = ode45(@RayleighPlesset,Toutput,[R01 0], options);

 [Todd_T, Todd_Y] = ode45(@Todd, Toutput, [R01 R02 0 0 1 -1 0 d], options); 

saveflag = 0;
set(0,'DefaultAxesFontSize',14);

% figure;
% subplot(2, 2, 1);
% plot(time, Todd_Y(:,1), time, Todd_Y(:,2), time, Y(:, 1), time, EQ_1D_Gumerov_Y(:,1));
% legend('Todd R_b', 'Todd R_p', 'Rayleigh-Plesset R(t)', 'Gumerov 1D R_b');
% 
% subplot(2, 2, 2);
% plot(time, Todd_Y(:,3), time, Todd_Y(:,4), time, Y(:, 2));
% legend('Todd w_b', 'Todd w_p', 'Rayleigh�-Plesset dR/dt');
% 
% subplot(2, 2, 3);
% plot(time, Todd_Y(:,5), time, Todd_Y(:,6));
% legend('Todd U_b', 'Todd U_p');
% 
% subplot(2, 2, 4);
% plot(time, Todd_Y(:,7), time, Todd_Y(:,8));
% legend('Todd r_b', 'Todd r_p');



figure;
plot(time, Todd_Y(:,1), '>', time, EQ_1D_Gumerov_Y(:,1), 'o', time, Y1(:, 1), time, Y2(:, 1), 'LineWidth', 2);
legend('Todd R(t)', 'Gumerov R(t)', 'Rayleigh-Plesset R(t)', 'Rayleigh-Plesset(*) R(t)');
% plot(time, Todd_Y(:,1), '>', time, EQ_3D_Gumerov_Y(:,1), 'o', time, Y1(:, 1), time, Y2(:, 1), 'LineWidth', 2);
% legend('Todd R(t)', 'Gumerov R(t)', 'Rayleigh-Plesset R(t)', 'Rayleigh-Plesset(*) R(t)');

figure;
plot(time, Todd_Y(:,3), '>', time, EQ_1D_Gumerov_Y(:,3), 'o', time, Y1(:, 2), time, Y2(:, 2),  'LineWidth', 2);
legend('Todd w', 'Gumerov w', 'Rayleigh-Plesset w', 'Rayleigh-Plesset(*) w');
% plot(time, Todd_Y(:,3), '>', time, EQ_3D_Gumerov_Y(:,3), 'o', time, Y1(:, 2), time, Y2(:, 2),  'LineWidth', 2);
% legend('Todd w', 'Gumerov w', 'Rayleigh-Plesset w', 'Rayleigh-Plesset(*) w');

figure;
plot(time, Todd_Y(:,5), '>', time, Todd_Y(:,6), 'o', time, EQ_1D_Gumerov_Y(:,5), time, EQ_1D_Gumerov_Y(:,6), 'LineWidth', 2);
legend('Todd U_b', 'Todd U_p', 'Gumerov U_b', 'Gumerov U_p');
% plot(time, Todd_Y(:,5), time, Todd_Y(:,6), time, EQ_3D_Gumerov_Y(:,5), time, EQ_3D_Gumerov_Y(:,8), 'LineWidth', 2);
% legend('Todd U_b', 'Todd U_p', 'Gumerov U_b', 'Gumerov U_p');

figure;
plot(time, Todd_Y(:,7), 'o', time, Todd_Y(:,8), 'o', time, EQ_1D_Gumerov_Y(:,7), time, EQ_1D_Gumerov_Y(:,8),   'LineWidth', 3);
legend('Todd r_b',  'Todd r_p',  'Gumerov r_b', 'Gumerov r_p');
% plot(time, Todd_Y(:,7), 'o', time, Todd_Y(:,8), 'o', time, EQ_3D_Gumerov_Y(:,11), time, EQ_3D_Gumerov_Y(:,14),   'LineWidth', 3);
% legend('Todd r_b',  'Todd r_p',  'Gumerov r_b', 'Gumerov r_p');




% figure;
% plot(time, abs(Y1D(:,3)),   time, abs(Y4(:,3)),  time, abs(Alter_Todd_Y(:,3)), 'LineWidth',2 );
% legend('U = |U_1| = |U_2| Todd 1D (equation 5)',     'U = |U_1| = |U_2| N.A. 1D (equation 7)',     'U = |U_1| = |U_2| Alter Todd (equation 8)');
% if saveflag ~= 0
%     saveas(gcf,'fig8.png');
% end;