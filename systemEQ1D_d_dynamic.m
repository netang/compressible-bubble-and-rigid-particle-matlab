function dy = systemEQ1D_d_dynamic( t, y )

global R01 R02 rho_l rho_p V02 P0 gamma sigma Pg0 Ap omega;

R1 = y(1);
dR1 = y(2);
U1 = y(3);
U2 = y(4);

r1 = y(5);
r2 = y(6);

d = r2 - r1;

% 
% p_inf=0;
% P1 = (Pg0*(R01/R1)^(3*gamma)-2*sigma/R1)-p_inf;

 p_inf= -Ap*sin(omega*t);
 P1 = (Pg0*(R01/R1)^(3*gamma)-2*sigma/R1)-p_inf;


dy = zeros(6, 1);

F = 3*rho_l*V02*(R1^2/d^3*dR1*U2+R1^3/d^4*U1*U2-R1^4/d^5*dR1^2);

A = [ R1+(R02^3*R1^2)/(2*d^4)      R02^3*R1^3/(2*d^5)         -R02^3/(2*d^2);
      3/2*rho_l*V02*R1^5 /(d^5)    2/3*rho_l*pi*R1^3         (-3/2)*rho_l*V02*(R1^3) / (d^3);
      (-3/2)*rho_l*V02*R1^2 /(d^2)    (-3/2)*rho_l*V02*R1^3 /(d^3)  (1/2*rho_l + rho_p)*V02;
    ];
% b = [ -3/2 * dR1^2 + (P1-P0)/rho_l + (1/4)*U1^2 - R02^3*U2*(U1+2*U2)/(2*d^3) - R02^3*R1*dR1^2 / d^4 - 2*R02^3*R1^2*dR1*(U1-U2) / d^5;  
b = [ -3/2 * dR1^2 + (P1-P0)/rho_l + (1/4)*U1^2 - R02^3*U2*(U1+2*U2)/(2*d^3) - R02^3*R1*dR1^2 / d^4 - 2*R02^3*R1^2*dR1*(U1-U2) / d^5;
     %-F
      -F - 2*rho_l*pi*R1^2 * dR1 * U1 + (3/2)*rho_l*V02* ...
      (3*R1^2*dR1*U2/d^3 + R1^3*U2*(-3*d^(-4)*(U2-U1)) - (5*R1^4*dR1^2/d^5 + R1^5*dR1*(-5*d^(-6)*(U2-U1))  ));
      %+F
      F + 3/2*rho_l*V02*(  2*R1*dR1^2/d^2 + R1^2 * dR1 * (-2*d^(-3)*(U2-U1)) ...
      + (3*R1^2*dR1*U1)/d^3 +  R1^3*U1*(-3*d^(-4)*(U2-U1))   )
      
    ];



x = A\b;

dy(1) = dR1;
dy(2) = x(1);
dy(3) = x(2);
dy(4) = x(3);

dy(5) = U1;
dy(6) = U2;


end

