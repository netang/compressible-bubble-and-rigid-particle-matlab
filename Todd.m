function dy = Todd( t, y  )
%Todd implementation for compress bubble and rigid particle

global R01 R02 rho_l rho_p P0 gamma sigma Pg0 Ap omega;

R1 = y(1);
R2 = y(2);
dR1 = y(3);
dR2 = y(4);
dR2 = 0;
U1 = y(5);
U2 = y(6);
r1 = y(7);
r2 = y(8);


d = abs(r2 - r1);

% p_inf = P0 - Ap*sin(omega*t);
p_inf = P0;
p_lb = Pg0*(R01/R1)^(3*gamma)-2*sigma/R1;
p_lp = Pg0*(R02/R2)^(3*gamma)-2*sigma/R2;

% rho = rho_p/rho_l;
rho = 0;

A = [  R1^5*R2^3/d^5           1/3*R1^3            0;
      -R2^3*R1^2/d^2           0                   1/3*R2^3*(1+2*(rho));
       R1+R2^3*R1^2/(2*d^4)    R2^3*R1^3/(2*d^5)  -R2^3/(2*d^2);
    ];

b = [  -R1^2*dR1*U1 - R1^2*R2/d^2*(R2*dR1*dR2 + 2*R1*dR2^2) ...
      + 1/2*R1^2*R2^2/d^3*( (5*R2*dR1+13*R1*dR2)*U2 -3*U2*(R2*dR1+R1*dR2) ) ...
      + 3/2*R1^3*R2^3/d^4*( 2*U2*(U1-U2) -2*U1*U2 ) ...
      - R1^3*R2^2/d^5*( 3*R1*R2*dR1^2 + 3*R1^2*dR1*dR2 - 2*R2^2*dR2^2 );
      
       -R2^2*dR2*U2 + R2^2*R1/d^2*(R1*dR2*dR1 + 2*R2*dR1^2) ...
      + 1/2*R2^2*R1^2/d^3*( (5*R1*dR2+13*R2*dR1)*U1 -3*U1*(R1*dR2+R2*dR1) ) ...
      + 3/2*R2^3*R1^3/d^4*( -2*U1*(U2-U1) +2*U2*U1 ) ...
      + R2^3*R1^2/d^5*( 3*R2*R1*dR2^2 + 3*R2^2*dR2*dR1 - 2*R1^2*dR1^2 );
      
       -3/2*dR1^2 + (p_lb - p_inf)/rho_l + 1/4*U1^2 - R2/d*(2*dR2^2) ...
      + R2^2/(2*d^2)*(dR2*(5*U2+U1)) - R2^3/(2*d^3)*U2*(U1+2*U2) ...
      - R2^2/(4*d^4)*(4*R1*R2*dR1^2 + 6*R1^2*dR1*dR2 - 3*R2^2*dR2^2 ) ...
      - R2^2/(2*d^5)*(3*R1^3*dR2*U1 + 3*R2^3*dR2*U2 + 4*R1^2*R2*dR1*(U1 - U2) );
      
    ];


X = A\b;

dy = zeros(8, 1);

dy(1) = dR1;
dy(2) = dR2;
dy(3) = X(1);
dy(4) = 0;%X(2);
dy(5) = X(2);
dy(6) = X(3);
dy(7) = U1;
dy(8) = U2;





end

