function dy = systemEQ3D_Gumerov( t, y )

global R01 R02 ap rho_l rho_p P0 gamma sigma Pg0 Ap omega;

ab = y(1);
ap = y(2);
wb = y(3);
wp = y(4);
wp = 0;
Ub = y(5:7);
Up = y(8:10);
rb = y(11:13);
rp = y(14:16);

U = Up - Ub;
d = rp - rb;
modul_d = sqrt(d'*d);
eps_b = ab/modul_d;
eps_p = ap/modul_d;
e = d/modul_d;
rho = rho_p/rho_l;

p_inf = P0 - Ap*sin(omega*t);
p_lb = Pg0*(R01/ab)^(3*gamma)-2*sigma/ab;
p_lp = Pg0*(R02/ap)^(3*gamma)-2*sigma/ap;

dy = zeros(16, 1);

Alb = -3/2*eps_p^2*(wp - eps_p*dot(-U,-e))*Up + eps_p^2*(wp+3/2*eps_p*dot(Up,-e))*-U ...
   + eps_p*(2*wp^2-3*eps_p*wp*dot(-U,-e) + 3/2*eps_p^2*dot(Up,-U)+9/2*eps_p*wp*dot(Up,-e)-15/2*eps_p^2*dot(Up,-e)*dot(-U,-e))*-e;
Alp = -3/2*eps_b^2*(wb - eps_b*dot(U,e))*Ub + eps_b^2*(wb+3/2*eps_b*dot(Ub,e))*U ...
   + eps_b*(2*wb^2-3*eps_b*wb*dot(U,e) + 3/2*eps_b^2*dot(Ub,U)+9/2*eps_b*wb*dot(Ub,e)-15/2*eps_b^2*dot(Ub,e)*dot(U,e))*e;


Ulb = (ap^2*wp*-d)/modul_d^3 - (ap^3*Up)/(2*modul_d^3) + (3*ap^3*dot(Up,-d)*-d)/(2*modul_d^5);%?
Ulp = (ab^2*wb*d)/modul_d^3 - (ab^3*Ub)/(2*modul_d^3) + (3*ab^3*dot(Ub,d)*d)/(2*modul_d^5);%?
Qb = -3*wb*(Ub-Ulb);%+
Qp = -3*wp*(Up-Ulp);%+
Qbv = -3/2*wb^2+1/4*dot(Ub-Ulb,Ub-Ulb) + (p_lb - p_inf)/rho_l;%+
% Qpv = -3/2*wp^2+1/4*dot(Up-Ulp,Up-Ulp) + (p_lp - p_inf)/rho_l;%+
Rb = 3*eps_b*Alb + Qb; %+
Rp = 3*eps_p*Alp + Qp; %+

C1 = 3/2*ab*eps_p^3;
C2 = -9/2*ab*eps_p^3;
C3 = 3/2*ap*eps_b^3;
C4 = -9/2*ap*eps_b^3;
A1 = diag([ab; ab; ab]);
A2 = zeros(3,3);
for i = 1:3
    for j = 1:3
        A2(i,j) = C2*e(i)*e(j);
    end
end
A2 = A2 + diag([C1; C1; C1]);


A3 = zeros(3,3);
for i = 1:3
    for j = 1:3
        A3(i,j) = C4*e(i)*e(j);
    end
end
A3 = A3 + diag([C3; C3; C3]);
A4 = diag([ap*(1+2*rho); ap*(1+2*rho); ap*(1+2*rho)]);%#


dUdt = [A1 A2; A3 A4]\[Rb; 3*ap*eps_b^2*(Qbv/ab)*e + Rp] ;  


dy(1) = wb;
dy(2) = wp;
dy(3) = Qbv/ab;
dy(4) = 0;%;Qpv/ap;
dy(5:7) = dUdt(1:3);
dy(8:10) = dUdt(4:6);
dy(11:13) = Ub;
dy(14:16) = Up;


end

