function dy = systemEQ3D( t, y )

global R01 ap rho_l rho_p P0 gamma sigma Pg0 Ap omega;

ab = y(1);
wb = y(2);
Ub = y(3:5);
Up = y(6:8);
rb = y(9:11);
rp = y(12:14);

U = Up - Ub;
d = rp - rb;
modul_d = sqrt(d'*d);
eps_b = ab/modul_d;
eps_p = ap/modul_d;
e = d/modul_d;
rho = rho_p/rho_l;



P1 = Pg0*(R01/ab)^(3*gamma)+Ap*sin(omega*t)-2*sigma/ab; %p_lb
Fob = [0; 0; 0];
Fop = [0; 0; 0];
dy = zeros(14, 1);


% Q01 = P1/rho_l - 3/2*wb^2+1/4*dot(Ub,Ub);
% Q1  = Q01 + 1/4*eps_p^3*(  dot(Ub,Up) - 3*dot(U,e)*dot(Ub,e)  );
% Q02 = -3*wb*Ub+3/(2*pi*rho_l*ab^2) * Fob;
Ulb = -1/2*eps_p^3*(Up-3*dot(Up,e)*e);
Ulb = -Ulb;

Q1 = -3/2*wb^2+1/4*dot(Ub-Ulb,Ub-Ulb)+(P1-P0)/rho_l;
Q2  = -3*wb*(Ub-Ulb);
Q3  = 3/(2*pi*rho_l*ab^2) * Fop;

%Alb = 3/2 * eps_p^3 * ( dot(U,e)*Up + (dot(Up,U) - 5*dot(Up,e)*dot(U,e))*e ) + 3/2*eps_p^3*dot(Up,e)*U;
Alb = 3/2 * eps_p^3*dot(U,e)*Up + 3/2*eps_p^3*dot(Up,e)*U + 3/2*eps_p^3*(dot(Up,U)-5*dot(Up,e)*dot(U,e))*e;
% Alp = -3/2* eps_b^2 * ( wb - eps_b*dot(U,e) )*Ub + eps_b^2*wb*U + ... eps_b^2*wb*U + ...
%     eps_b*( 2*wb^2-3*eps_b*wb*dot(U,e)+9/2*eps_b*wb*dot(Ub,e) + 3/2*eps_b^2*dot(Ub,U) - ...
%     15/2*eps_b^2*dot(Ub,e)*dot(U,e))*e + 3/2*eps_b^3*dot(Ub,e)*U;
Alp = -3/2*eps_b^2*(wb - eps_b*dot(U,e))*Ub + eps_b^2*(wb+3/2*eps_b*dot(Ub,e))*U ...
   + eps_b*(2*wb^2-3*eps_b*wb*dot(U,e) + 3/2*eps_b^2*dot(Ub,U)+9/2*eps_b*wb*dot(Ub,e)-15/2*eps_b^2*dot(Ub,e)*dot(U,e))*e;

%             ab*dUb   + 3/2*ab*eps_p^3*dUp = 9/2*ab*eps_p^3*(  (3*eps_b*eps_p*Q1+3*epsp*dot(e,Alp)+dot(e,Q3))/((1+2*rho)*ap)  )*e + 3*eps_b*Alb + Q2;
% 3/2*ap*eps_b^3*dUb   +   (1+2*rho)*ap*dUp = 9/2*ap*eps_b^3*(  (3*eps_b*dot(e,Alb)+dot(e,Q2))/ab  )*e + 3*eps_p*Alp + Q3 + 3*ap*eps_b^2*(dwbdt)*e;
C1 = 3/2*ab*eps_p^3;
C1 = -C1;
C2 = -9/2*ab*eps_p^3;
C2 = -C2;
C3 = 3/2*ap*eps_b^3;
C4 = -9/2*ap*eps_b^3;
A1 = diag([ab; ab; ab]);
A2 = zeros(3,3);
for i = 1:3
    for j = 1:3
        A2(i,j) = C2*e(i)*e(j);
    end
end
A2 = A2 + diag([C1; C1; C1]);


A3 = zeros(3,3);
for i = 1:3
    for j = 1:3
        A3(i,j) = C4*e(i)*e(j);
    end
end
A3 = A3 + diag([C3; C3; C3]);
A4 = diag([(1+2*rho)*ap; (1+2*rho)*ap; (1+2*rho)*ap]);


dUdt = [A1 A2; A3 A4]\[-3*eps_b*Alb + Q2; (3*eps_p*Alp + Q3 + 3*ap*eps_b^2*(Q1/ab)*e)] ;     

dy(1) = wb;
dy(2) = Q1/ab;
dy(3:5) = dUdt(1:3);
dy(6:8) = dUdt(4:6);
dy(9:11) = Ub;
dy(12:14) = Up;


end

